public class Main {

    /**
     *
     * time java -Xint Main
     * time java -Djava.compiler=NONE Main
     *
     * -XX:CompileThreshold=<n>：该参数指定JIT编译器触发编译的方法调用次数，默认值为10000，可以根据应用程序的实际情况进行调整。
     * @param args
     */
    public static void main(String[] args) {
        long sum = 0L;
        for (long i = 0; i < 1000000000L; i++) {
            sum += i;
        }
        System.out.println(sum);
    }

}